package TC_Runner;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Utilities.GetTCDetailsFromDataSheet;
import Utilities.Reporting_Utilities;
import Utilities.getCurrentDate;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Runner_MDMP_225_Validate_After_RMS_batch_file_to_S3 {

	@Test
	public void test_MDMP_225_Validate_After_RMS_batch_file_to_S3() throws IOException {

		ExtentReports extent = null;
		ExtentTest logger = null;
		String ResultPath = null;
		XWPFRun xwpfRun = null;
		XWPFDocument doc = null;

		String Final_Result = "FAIL";

		String TestKeyword = "Batch_S3_File_Upload";

		String TestCaseNo = GetTCDetailsFromDataSheet.TCDetails(TestKeyword)
				.get("TestCaseNo");
		String TestCaseName = GetTCDetailsFromDataSheet.TCDetails(TestKeyword)
				.get("TestCaseName");

		if (ResultPath == null) {

			String ResPath = System.getProperty("user.dir") + "/Results" + "/"
					+ "Batch_S3_Upload_" + getCurrentDate.getISTDateddMM();

			ResultPath = Reporting_Utilities.createResultPath(ResPath);
			// ResultPath=Reporting_Utilities.createResultPath("NAS "+getCurrentDate.getISTDateddMM()+"/run_RMSE_RPAS_ITEM_MASTER");

			ResultPath.replace("\\", "/");
			System.setProperty("resultpath", ResultPath);
		}

		System.out.println("Result path is " + ResultPath);

		String TC_ScFolder = ResultPath + TestCaseNo;

		System.out.println("Sc result path is " + TC_ScFolder);

		File dir = new File(TC_ScFolder);

		if (!dir.exists()) {

			dir.mkdir();

		}

		try {

			doc = new XWPFDocument();
			XWPFParagraph p = doc.createParagraph();
			xwpfRun = p.createRun();

			extent = new ExtentReports(
					ResultPath + "Batch_S3_File_Upload_SeleniumReport.html", false);
			extent.loadConfig(new File(System.getProperty("user.dir")
					+ "\\extent-config.xml"));
			// //*******************************************************************************************************
			// //****************************Instantiate Extent
			// Reporting************************
			logger = extent.startTest(TestCaseNo + ":" + TestCaseName);

			MDMP_225_Validate_After_RMS_batch_file_to_S3.TC_00_Step1_ExecuteJob_ITEM_MASTER_FILE_PROC_ITEM
					.Prepare(extent, logger, ResultPath, TC_ScFolder, xwpfRun);
			Result ResStep1 = JUnitCore
					.runClasses(MDMP_225_Validate_After_RMS_batch_file_to_S3.TC_00_Step1_ExecuteJob_ITEM_MASTER_FILE_PROC_ITEM.class);

			boolean step1 = ResStep1.wasSuccessful();

			MDMP_225_Validate_After_RMS_batch_file_to_S3.TC_00_Step2_Batch_Navigate_To_S3
					.Prepare(extent, logger, ResultPath, TC_ScFolder, xwpfRun);

			Result ResStep2 = JUnitCore
					.runClasses(MDMP_225_Validate_After_RMS_batch_file_to_S3.TC_00_Step2_Batch_Navigate_To_S3.class);

			boolean step2 = ResStep2.wasSuccessful();

			

			if (step1 && step2 ) {
//				if (step2) {

				Final_Result = "PASS";
				logger.log(LogStatus.PASS, "TestCaseNo::" + TestCaseName
						+ ":: PASS");

				Assert.assertTrue(TestCaseNo + "--" + TestCaseName, true);

			}

			else {

				logger.log(LogStatus.FAIL, "TestCaseNo::" + TestCaseName
						+ ":: FAIL");
				Assert.assertTrue(TestCaseNo + "--" + TestCaseName, false);

			}

		}

		catch (Exception e) {

			System.out.println("The exception is " + e);

		}

		finally {

			extent.endTest(logger);
			extent.flush();

			FileOutputStream out1;

			out1 = new FileOutputStream(TC_ScFolder + "/"
					+ "Batch_S3_File_Upload_" + Final_Result + ".docx");
			doc.write(out1);
			out1.close();
			doc.close();

		}

	}

}
