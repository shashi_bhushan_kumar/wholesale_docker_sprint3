package Utilities;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class GetTCDetailsFromDataSheet {

	public static Map<String, String> TCDetails(String TestKeyword) {

		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		Map<String, String> mapTcDetails = null;
		String TestDataPath = ProjectConfigurations
				.LoadProperties("TestDataPath");
		String SheetName = ProjectConfigurations.LoadProperties("SheetName");

		try

		{

			int rows = 0;
			int occurances = 0;

			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);

			Map<String, Integer> map = new HashMap<String, Integer>();

			mapTcDetails = new HashMap<String, String>();

			for (int i = 0; i < FirstRow.length; i++) {
				map.put(FirstRow[i].getContents().trim(), i);

			}

			for (int r = 1; r < rows; r++) {

				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents()
						.trim();
				if (Keyword.equalsIgnoreCase(TestKeyword)) {

					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r)
							.getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r)
							.getContents().trim();

					// System.out.println("TC no is "+TestCaseNo);
					// System.out.println("Tc name  is "+TestCaseName);

					mapTcDetails.put("TestCaseNo", TestCaseNo);
					mapTcDetails.put("TestCaseName", TestCaseName);

					occurances = occurances + 1;

				}

				if (occurances > 0) {
					break;
				}

			}

		}

		catch (Exception ex) {
			System.out
					.println("Exception occurred while fetching Testcase No and TestCase name from data sheet");
			mapTcDetails = null;

		}

		finally {

			return mapTcDetails;

		}

	}

}