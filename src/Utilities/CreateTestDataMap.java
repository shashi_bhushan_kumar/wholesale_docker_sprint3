package Utilities;

import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;

public class CreateTestDataMap {

	public static Map<String, Integer> createReturnTestData(Sheet sheet) {
		// **************Reading Header values from test data
		// sheet***************************

		Cell[] FirstRow = sheet.getRow(0);
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < FirstRow.length; i++) {
			map.put(FirstRow[i].getContents().trim(), i);
		}
		return map;
	}

}
