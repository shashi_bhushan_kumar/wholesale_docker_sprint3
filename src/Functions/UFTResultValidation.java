package Functions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.MyException;
import Utilities.excelCellValueWrite;
import Utilities.utilityFileWriteOP;

public class UFTResultValidation {

	public static boolean UFTScrpitFiveStepsValidation(int r, String tcid,
			String DriverSheetPath, String SheetName, String ResultPath,
			XWPFRun xwpfRun) throws IOException, InterruptedException,
			MyException {
		Thread.sleep(10000);
		boolean Final_Result = false;
		try {
			File directory = new File("C:\\Morrisons Automation\\Results");

			File[] files = directory.listFiles();

			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			System.out
					.println("\nLast Modified Descending Order (LASTMODIFIED_REVERSE)");

			System.out.println(files[0].getAbsolutePath());
			if (files[0].isFile()) {

				InputStream ExcelFileToRead = new FileInputStream(
						files[0].getPath());
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
				String ExtractFileBatchRunStatus = wb.getSheetAt(0).getRow(2)
						.getCell(3).toString();
				String FileAvailabilityInOretail = wb.getSheetAt(0).getRow(5)
						.getCell(3).toString();
				System.out.println(FileAvailabilityInOretail);
				String NASCopyBatchRunStatus = wb.getSheetAt(0).getRow(8)
						.getCell(3).toString();
				String FileAvailabilityInNasStatus = wb.getSheetAt(0)
						.getRow(11).getCell(3).toString();
				String UploadToS3BatchRun = wb.getSheetAt(0).getRow(14)
						.getCell(3).toString();

				if (ExtractFileBatchRunStatus.trim().equalsIgnoreCase("PASS")) {
					String Result = wb.getSheetAt(0).getRow(2).getCell(2)
							.toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
							ResultPath, xwpfRun, "");
					if (FileAvailabilityInOretail.equalsIgnoreCase("PASS")) {
						Result = wb.getSheetAt(0).getRow(5).getCell(2)
								.toString();
						utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
								ResultPath, xwpfRun, "");

						String Filename = Result.substring(Result
								.indexOf("File name is:") + 14);
						System.out.println(Filename);
						excelCellValueWrite.writeValueToCell(Filename, r, 28,
								DriverSheetPath, SheetName);

						if (NASCopyBatchRunStatus.equalsIgnoreCase("PASS")) {
							Result = wb.getSheetAt(0).getRow(8).getCell(2)
									.toString();
							utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
									ResultPath, xwpfRun, "");
							if (FileAvailabilityInNasStatus
									.equalsIgnoreCase("PASS")) {
								Result = wb.getSheetAt(0).getRow(11).getCell(2)
										.toString();
								utilityFileWriteOP.writeToLog(tcid, Result,
										"PASS", ResultPath, xwpfRun, "");
								if (UploadToS3BatchRun.equalsIgnoreCase("PASS")) {
									Result = wb.getSheetAt(0).getRow(14)
											.getCell(2).toString();
									utilityFileWriteOP.writeToLog(tcid, Result,
											"PASS", ResultPath, xwpfRun, "");
								} else {
									Result = wb.getSheetAt(0).getRow(14)
											.getCell(2).toString();
									utilityFileWriteOP.writeToLog(tcid, Result,
											"FAIL", ResultPath, xwpfRun, "");
								}
							} else {
								Result = wb.getSheetAt(0).getRow(11).getCell(2)
										.toString();
								utilityFileWriteOP.writeToLog(tcid, Result,
										"FAIL", ResultPath, xwpfRun, "");
							}
						} else {
							Result = wb.getSheetAt(0).getRow(8).getCell(2)
									.toString();
							utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
									ResultPath, xwpfRun, "");
						}
					} else {
						Result = wb.getSheetAt(0).getRow(5).getCell(2)
								.toString();
						utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
								ResultPath, xwpfRun, "");
					}
				} else {

					utilityFileWriteOP.writeToLog(tcid, wb.getSheetAt(0)
							.getRow(2).getCell(2).toString(), "FAIL",
							ResultPath, xwpfRun, "");
				}

				if (ExtractFileBatchRunStatus.equalsIgnoreCase("PASS")
						&& FileAvailabilityInOretail.equalsIgnoreCase("PASS")
						&& NASCopyBatchRunStatus.equalsIgnoreCase("PASS")
						&& FileAvailabilityInNasStatus.equalsIgnoreCase("PASS")
						&& UploadToS3BatchRun.equalsIgnoreCase("PASS")) {
					Final_Result = true;
				} else {
					Final_Result = false;
				}
			} else {
				Final_Result = false;
				utilityFileWriteOP.writeToLog(tcid,
						"Result Excel file not found instead got a folder",
						"FAIL", ResultPath, xwpfRun, "");
				utilityFileWriteOP
						.writeToLog(
								tcid,
								"Error occured in UFT Script, Check the error in UFT logs",
								"FAIL", ResultPath, xwpfRun, "");
			}
		} catch (Exception e) {
			Final_Result = false;
			utilityFileWriteOP.writeToLog(tcid,
					"Error occured in UFT Script, Check the error in UFT logs",
					"FAIL", ResultPath, xwpfRun, "");
			return Final_Result;
		}

		return Final_Result;

	}

	public static boolean UFTScriptOneStepValidation(int r, String tcid,
			String DriverSheetPath, String SheetName, String ResultPath,
			XWPFRun xwpfRun) throws IOException, InterruptedException,
			MyException {
		Thread.sleep(10000);
		boolean Final_Result = false;
		try {
			File directory = new File("C:\\Morrisons Automation\\Results");

			File[] files = directory.listFiles();

			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			if (files[0].isFile()) {

				InputStream ExcelFileToRead = new FileInputStream(
						files[0].getPath());
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
				String ResultUfT = wb.getSheetAt(0).getRow(2).getCell(3)
						.toString();
				System.out.println("Required String:" + ResultUfT);

				if (ResultUfT.equalsIgnoreCase("PASS")) {
					Final_Result = true;
					String Result = wb.getSheetAt(0).getRow(2).getCell(2)
							.toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
							ResultPath, xwpfRun, "");

				} else if (ResultUfT.equalsIgnoreCase("FAIL")) {

					Final_Result = false;
					String Result = wb.getSheetAt(0).getRow(2).getCell(2)
							.toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "Fail",
							ResultPath, xwpfRun, "");
					throw new MyException("Job Aborted");

				}
			} else {
				Final_Result = false;

				utilityFileWriteOP.writeToLog(tcid,
						"UFT Script failed somewhere in between", "FAIL",
						ResultPath, xwpfRun, "");
				utilityFileWriteOP.writeToLog(tcid,
						"Please refer to UFT log for error debugging", "FAIL",
						ResultPath, xwpfRun, "");
				throw new MyException("UFT Script failed somewhere in between");

			}

		} catch (Exception e) {
			Final_Result = false;
			utilityFileWriteOP.writeToLog(tcid,
					"Error occured in UFT Script, Check the error in UFT logs",
					"FAIL", ResultPath, xwpfRun, "");
			return Final_Result;
		}

		return Final_Result;
	}

	public static boolean UFTScrpitFourStepsValidation(int r, String tcid,
			String DriverSheetPath, String SheetName, String ResultPath,
			XWPFRun xwpfRun) throws IOException, InterruptedException,
			MyException {
		Thread.sleep(10000);
		boolean Final_Result = false;
		try {
			File directory = new File("C:\\Morrisons Automation\\Results");

			File[] files = directory.listFiles();

			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			System.out
					.println("\nLast Modified Descending Order (LASTMODIFIED_REVERSE)");

			System.out.println(files[0].getAbsolutePath());
			if (files[0].isFile()) {

				InputStream ExcelFileToRead = new FileInputStream(
						files[0].getPath());
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
				String FileAvailabilityInOretail = wb.getSheetAt(0).getRow(2)
						.getCell(3).toString();
				String NASCopyBatchRunStatus = wb.getSheetAt(0).getRow(5)
						.getCell(3).toString();
				String FileAvailabilityInNasStatus = wb.getSheetAt(0).getRow(8)
						.getCell(3).toString();
				String UploadToS3BatchRun = wb.getSheetAt(0).getRow(11)
						.getCell(3).toString();

				String Result = null;
				if (FileAvailabilityInOretail.equalsIgnoreCase("PASS")) {
					Result = wb.getSheetAt(0).getRow(2).getCell(2).toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
							ResultPath, xwpfRun, "");

					String Filename = Result.substring(Result
							.indexOf("File name is:") + 14);
					System.out.println(Filename);
					excelCellValueWrite.writeValueToCell(Filename, r, 28,
							DriverSheetPath, SheetName);

					if (NASCopyBatchRunStatus.equalsIgnoreCase("PASS")) {
						Result = wb.getSheetAt(0).getRow(5).getCell(2)
								.toString();
						utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
								ResultPath, xwpfRun, "");

						if (FileAvailabilityInNasStatus
								.equalsIgnoreCase("PASS")) {
							Result = wb.getSheetAt(0).getRow(8).getCell(2)
									.toString();
							utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
									ResultPath, xwpfRun, "");

							if (UploadToS3BatchRun.equalsIgnoreCase("PASS")) {
								Result = wb.getSheetAt(0).getRow(11).getCell(2)
										.toString();
								utilityFileWriteOP.writeToLog(tcid, Result,
										"PASS", ResultPath, xwpfRun, "");
							} else {
								Result = wb.getSheetAt(0).getRow(11).getCell(2)
										.toString();
								utilityFileWriteOP.writeToLog(tcid, Result,
										"FAIL", ResultPath, xwpfRun, "");
							}
						} else {
							Result = wb.getSheetAt(0).getRow(8).getCell(2)
									.toString();
							utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
									ResultPath, xwpfRun, "");
						}
					} else {
						Result = wb.getSheetAt(0).getRow(5).getCell(2)
								.toString();
						utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
								ResultPath, xwpfRun, "");
					}
				} else {
					Result = wb.getSheetAt(0).getRow(2).getCell(2).toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
							ResultPath, xwpfRun, "");
				}

				if (FileAvailabilityInOretail.equalsIgnoreCase("PASS")
						&& NASCopyBatchRunStatus.equalsIgnoreCase("PASS")
						&& FileAvailabilityInNasStatus.equalsIgnoreCase("PASS")
						&& UploadToS3BatchRun.equalsIgnoreCase("PASS")) {
					Final_Result = true;
				} else {
					Final_Result = false;
				}
			} else {
				Final_Result = false;
				utilityFileWriteOP.writeToLog(tcid,
						"Result Excel file not found instead got a folder",
						"FAIL", ResultPath, xwpfRun, "");
				utilityFileWriteOP
						.writeToLog(
								tcid,
								"Error occured in UFT Script, Check the error in UFT logs",
								"FAIL", ResultPath, xwpfRun, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Final_Result = false;
			utilityFileWriteOP.writeToLog(tcid,
					"Error occured in UFT Script, Check the error in UFT logs",
					"FAIL", ResultPath, xwpfRun, "");
			return Final_Result;
		}

		return Final_Result;

	}

	public static boolean UFTScrpitADSRangeValidation(int r, String tcid,
			String DriverSheetPath, String SheetName, String ResultPath,
			XWPFRun xwpfRun) throws IOException, InterruptedException,
			MyException {
		Thread.sleep(10000);
		boolean Final_Result = false;
		try {
			File directory = new File("C:\\Morrisons Automation\\Results");

			File[] files = directory.listFiles();

			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			System.out
					.println("\nLast Modified Descending Order (LASTMODIFIED_REVERSE)");

			System.out.println(files[0].getAbsolutePath());
			if (files[0].isFile()) {

				InputStream ExcelFileToRead = new FileInputStream(
						files[0].getPath());
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
				String MainFrameBatchStatus = wb.getSheetAt(0).getRow(2)
						.getCell(3).toString();
				String MainFrameJob = wb.getSheetAt(0).getRow(3).getCell(3)
						.toString();
				String FileAvailabilityInADSNas = wb.getSheetAt(0).getRow(6)
						.getCell(3).toString();

				String Result = null;
				if (MainFrameBatchStatus.equalsIgnoreCase("PASS")) {
					Result = wb.getSheetAt(0).getRow(2).getCell(2).toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
							ResultPath, xwpfRun, "");

					if (MainFrameJob.equalsIgnoreCase("PASS")) {
						Result = wb.getSheetAt(0).getRow(3).getCell(2)
								.toString();
						utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
								ResultPath, xwpfRun, "");

						if (FileAvailabilityInADSNas.equalsIgnoreCase("PASS")) {
							Result = wb.getSheetAt(0).getRow(6).getCell(2)
									.toString();
							utilityFileWriteOP.writeToLog(tcid, Result, "PASS",
									ResultPath, xwpfRun, "");
							String Filename = Result.substring(Result
									.indexOf("File name is:") + 14);
							System.out.println(Filename);
							excelCellValueWrite.writeValueToCell(Filename, r,
									28, DriverSheetPath, SheetName);
						} else {
							Result = wb.getSheetAt(0).getRow(6).getCell(2)
									.toString();
							utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
									ResultPath, xwpfRun, "");
						}
					} else {
						Result = wb.getSheetAt(0).getRow(3).getCell(2)
								.toString();
						utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
								ResultPath, xwpfRun, "");
					}
				} else {
					Result = wb.getSheetAt(0).getRow(2).getCell(2).toString();
					utilityFileWriteOP.writeToLog(tcid, Result, "FAIL",
							ResultPath, xwpfRun, "");
				}

				if (FileAvailabilityInADSNas.equalsIgnoreCase("PASS")
						&& MainFrameJob.equalsIgnoreCase("PASS")
						&& MainFrameBatchStatus.equalsIgnoreCase("PASS")) {
					Final_Result = true;
				} else {
					Final_Result = false;
				}
			} else {
				Final_Result = false;
				utilityFileWriteOP.writeToLog(tcid,
						"Result Excel file not found instead got a folder",
						"FAIL", ResultPath, xwpfRun, "");
				utilityFileWriteOP
						.writeToLog(
								tcid,
								"Error occured in UFT Script, Check the error in UFT logs",
								"FAIL", ResultPath, xwpfRun, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Final_Result = false;
			utilityFileWriteOP.writeToLog(tcid,
					"Error occured in UFT Script, Check the error in UFT logs",
					"FAIL", ResultPath, xwpfRun, "");
			return Final_Result;
		}

		return Final_Result;

	}

}
