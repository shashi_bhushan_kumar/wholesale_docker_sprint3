package Functions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import Utilities.FolderZipper;
import Utilities.excelCellValueWrite;
import Utilities.getCurrentDate;
import Utilities.utilityFileWriteOP;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class WinSCPCall {

	public static boolean DownloadFileFromNAS_PO5(String us, String pw,
			String filename, String DownloadPath_NAS, String NASpath,
			String host, String port, String tcid, String ResultPath,
			XWPFRun xwpfRun, ExtentTest logger) {
		boolean res = false;
		String extension = null;

		if (filename.startsWith("item")) {
			extension = ".dat";
		}

		String matchpattern = filename;

		// itlext

		// String matchpattern="itlext.*";

		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();

			channel = session.openChannel("sftp");
			channel.connect();

			System.out.println("Connection Successful");

			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NASpath);
			System.out.println("The Current path is " + NASpath);

			Vector<ChannelSftp.LsEntry> list = null;

			try {
				list = sftpchannel.ls(matchpattern);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("File " + filename + " not found in "
						+ NASpath);
				utilityFileWriteOP.writeToLog(tcid, "File " + filename
						+ " is not found in  ", NASpath + " Download Failed",
						ResultPath, xwpfRun, "");

				res = false;
				return res;

			}

			for (ChannelSftp.LsEntry entry : list) {

				System.out.println(sftpchannel.pwd());

				String fName = entry.getFilename();

				if (fName.contains(matchpattern)) {

					sftpchannel.get(fName, DownloadPath_NAS + fName);// Copy
																		// file
																		// from
																		// WinSCP
																		// to
																		// local
					System.out.println("File Download Successful");

					// String extractfilename = fName.substring(0,
					// fName.lastIndexOf("."));

					// String archive_outbound_files =
					// getFilelistforaws("balances", fName, extractfilename);

					res = true;
					utilityFileWriteOP.writeToLog(tcid, filename
							+ " File is downloaded successfully from  ",
							NASpath + " to the location " + DownloadPath_NAS,
							ResultPath, xwpfRun, "");
					break;

				}

				else {

					System.out.println("File " + filename + " not found in "
							+ NASpath);
					utilityFileWriteOP.writeToLog(tcid, "File " + filename
							+ " is not found in  ", NASpath
							+ " Download Failed", ResultPath, xwpfRun, "");

				}

			}

		} catch (Exception e) {

			System.out.println("File Transfer Failed " + e);
			utilityFileWriteOP.writeToLog(tcid,
					"Exception occurred while downloading  file " + filename,
					" from  " + NASpath, ResultPath, xwpfRun, "");

			e.printStackTrace();
			res = false;
			return res;
		} finally {
			channel.disconnect();
			session.disconnect();
			return res;

		}
	}

	public static boolean DownloadFileFrom_Oretail(String us, String pw,
			String filename, String Oretail_DownloadPath, String OretailPath,
			String host, String port, String tcid, String ResultPath,
			XWPFRun xwpfRun, ExtentTest logger) {
		boolean res = false;
		String extension = null;

		String matchpattern = filename;

		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();

			channel = session.openChannel("sftp");
			channel.connect();

			System.out.println("Connection Successful");

			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(OretailPath);
			System.out.println("The Current path is -> " + OretailPath);

			Vector<ChannelSftp.LsEntry> list = null;

			try {
				list = sftpchannel.ls(matchpattern);

			}

			catch (Exception e) {
				// TODO: handle exception
				System.out.println("File " + filename + " not found in "
						+ OretailPath);
				utilityFileWriteOP.writeToLog(tcid, "File " + filename
						+ " is not found in  ", OretailPath
						+ " Download Failed", ResultPath, xwpfRun, "");

				res = false;

				return res;

			}

			for (ChannelSftp.LsEntry entry : list) {

				System.out.println(sftpchannel.pwd());

				String fName = entry.getFilename();

				if (fName.contains(matchpattern)) {

					// logger.log(LogStatus.PASS, "File matched");

					sftpchannel.get(fName, Oretail_DownloadPath + fName);// Copy
																			// file
																			// from
																			// WinSCP
																			// to
																			// local

					System.out.println("File Download Successful");

					// String extractfilename = fName.substring(0,
					// fName.lastIndexOf("."));

					// String archive_outbound_files =
					// getFilelistforaws("balances", fName, extractfilename);

					res = true;

					// utilityFileWriteOP.writeToLog(tcid,
					// filename+" File is downloaded successfully from  ",
					// OretailPath +" to the location "+Oretail_DownloadPath,
					// ResultPath,xwpfRun,"");

					break;

				}

				else {

					System.out.println("File " + filename + " not found in "
							+ OretailPath);

					utilityFileWriteOP.writeToLog(tcid, "File " + filename
							+ " is not found in  ", OretailPath
							+ " Download Failed", ResultPath, xwpfRun, "");

				}

			}

		} catch (Exception e) {

			System.out.println("File Transfer Failed " + e);

			utilityFileWriteOP.writeToLog(tcid,
					"Exception occurred while downloading  file " + filename,
					" from  " + OretailPath, ResultPath, xwpfRun, "");

			e.printStackTrace();
			res = false;

		} finally {
			channel.disconnect();
			session.disconnect();

			return res;

		}
	}

	public static boolean DownloadFileFromSFTP_PO5(String us, String pw,
			String filename, String DownloadPath_SFTP, String SFTPpath,
			String host, String port, String tcid, String ResultPath,
			XWPFRun xwpfRun, ExtentTest logger) {
		boolean res = false;
		String extension = null;

		File file = new File(DownloadPath_SFTP);

		if (!file.exists()) {
			file.mkdir();
			System.out.println("Directory is created!");
		} else {
			System.out.println("Directory is already existed!");
		}

		if (filename.startsWith("openorders")) {
			extension = ".dat";
		}

		String matchpattern = filename;

		// itlext

		// String matchpattern="itlext.*";

		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();

			channel = session.openChannel("sftp");
			channel.connect();

			System.out.println("Connection Successful");

			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(SFTPpath);
			System.out.println("The Current path is " + SFTPpath);

			Vector<ChannelSftp.LsEntry> list = null;

			try {
				list = sftpchannel.ls(matchpattern);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("File " + filename + " not found in "
						+ SFTPpath);
				utilityFileWriteOP.writeToLog(tcid, "File " + filename
						+ " is not found in  ", SFTPpath + " Download Failed",
						ResultPath, xwpfRun, "");

				res = false;
				return res;

			}

			for (ChannelSftp.LsEntry entry : list) {

				System.out.println(sftpchannel.pwd());

				String fName = entry.getFilename();

				if (fName.contains(matchpattern)) {

					sftpchannel.get(fName, DownloadPath_SFTP + fName);// Copy
																		// file
																		// from
																		// WinSCP
																		// to
																		// local
					System.out.println("File Download Successful");

					// String extractfilename = fName.substring(0,
					// fName.lastIndexOf("."));

					// String archive_outbound_files =
					// getFilelistforaws("balances", fName, extractfilename);

					res = true;
					utilityFileWriteOP.writeToLog(tcid, filename
							+ " File is downloaded successfully from  ",
							SFTPpath + " to the location " + DownloadPath_SFTP,
							ResultPath, xwpfRun, "");
					break;

				}

				else {

					System.out.println("File " + filename + " not found in "
							+ SFTPpath);
					utilityFileWriteOP.writeToLog(tcid, "File " + filename
							+ " is not found in  ", SFTPpath
							+ " Download Failed", ResultPath, xwpfRun, "");

				}

			}

		} catch (Exception e) {

			System.out.println("File Transfer Failed " + e);
			utilityFileWriteOP.writeToLog(tcid,
					"Exception occurred while downloading  file " + filename,
					" from  " + SFTPpath, ResultPath, xwpfRun, "");

			e.printStackTrace();
			res = false;
			return res;
		} finally {
			channel.disconnect();
			session.disconnect();
			return res;

		}
	}

	public static boolean nasFileValidate_WS2(String us, String pw,
			String filenames, String filePath, String NASpath, String host,
			String port, String tcid, String ResultPath, XWPFRun xwpfRun)
			throws IOException {

		boolean res = false;
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;

		String InvoiceID = null;

		String Result = null;
		int flag = 0;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();

			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NASpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			// w-mcc-OrderID.csv
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");

			// Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(filenames);

			for (ChannelSftp.LsEntry entry : list) {

				String filename = entry.getFilename();

				System.out.println("The current file is " + filename);
				sftpchannel.get(filename, filePath + filenames);// Copy file
																// from WinSCP
																// to local

				/*
				 * 
				 * //Read from local br = new BufferedReader(new
				 * FileReader(filePath + filename));
				 * 
				 * 
				 * String CurrentLine = null;
				 * 
				 * while((CurrentLine = br.readLine()) != null) {
				 * 
				 * //Split the Current line in comma seperated values String[]
				 * strarr = CurrentLine.split(",");
				 * 
				 * if(strarr[1].equals(OrderID) && strarr[2].equals(itemID) &&
				 * strarr[6].equals(Quantity)) {
				 * 
				 * 
				 * System.out.println("The NAS File is validated for order: " +
				 * OrderID+" of item: "+itemID+" and quantity "+Quantity);
				 * utilityFileWriteOP.writeToLog(tcid,
				 * "The NAS File is validated for order: " +
				 * OrderID+" of item: "+itemID+" and quantity "+Quantity,
				 * "PASS",ResultPath);
				 * 
				 * 
				 * InvoiceID = strarr[11];
				 * System.out.println("The Invoice id is " + InvoiceID);
				 * utilityFileWriteOP.writeToLog(tcid,
				 * "The Invoice for OrderID "+OrderID+"is ", InvoiceID); Result
				 * = InvoiceID;
				 * 
				 * 
				 * flag = 1; break; }
				 * 
				 * 
				 * 
				 * }
				 * 
				 * if(flag == 1) {
				 * 
				 * res=true; br.close(); break;
				 * 
				 * }
				 */

			}

			// br.close();
			sftpchannel.exit();

			session.disconnect();

			res = true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

		return res;
	}

	// ******To get the latest file extract generated after batch within 5
	// mins(latest file )

	public static String oretailLatestFileExtract(String us, String pw,
			String oretailpath, String filePrefix, String host, String port,
			String tcid, String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		String res = "";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(oretailpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			// w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("nb_blueyonder_"
					+ filePrefix + "_*.dat*");

			String filename = "";

			int p = 0;

			for (ChannelSftp.LsEntry entry : list) {

				filename = entry.getFilename();

				System.out.println("The current file is " + filename);

				// Getting the current date
				Date time = new Date();
				// This method returns the time in millis
				long timeMilli = time.getTime();

				System.out.println("The current time in miiliseconds:"
						+ timeMilli);

				Calendar calendar = Calendar.getInstance();
				System.out.println("Current Date = " + calendar.getTime());
				System.out.println("Time in milliseconds before= "
						+ (long) calendar.getTimeInMillis());

				// Subtract 15 minutes from current date
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(Calendar.MINUTE, -5);
				System.out.println("Updated Date = " + calendar1.getTime());
				System.out.println("Time in milliseconds after = "
						+ (long) calendar1.getTimeInMillis());

				long currentfilelastmodifiedmilliseconds = (long) entry
						.getAttrs().getMTime() * 1000L;

				System.out.println("The current file time is:"
						+ currentfilelastmodifiedmilliseconds);

				if (currentfilelastmodifiedmilliseconds < calendar
						.getTimeInMillis()
						&& calendar1.getTimeInMillis() < currentfilelastmodifiedmilliseconds) {

					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;
					res = filename;
					// utilityFileWriteOP.writeToLog(tcid,
					// "The target file name in "+oretailpath,
					// " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");

					System.out.println("The file is :" + filename);

					break;
				} else {

					System.out.println("There are no file generated recently");

					res = null;
				}
			}

			// br.close();
			sftpchannel.exit();

			session.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			// utilityFileWriteOP.writeToLog(tcid,
			// "Error occurred during File Extraction ", "Due to :"+e);
			return res;
		}

		return res;
	}

	public static String NASLatestFileExtract(String us, String pw,
			String oretailpath, String filePrefix, String host, String port,
			String tcid, String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		String res = "";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(oretailpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			// w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("nb_blueyonder_"
					+ filePrefix + "_*.dat*");

			String filename = "";

			int p = 0;

			for (ChannelSftp.LsEntry entry : list) {

				filename = entry.getFilename();

				System.out.println("The current file is " + filename);

				// Getting the current date
				Date time = new Date();
				// This method returns the time in millis
				long timeMilli = time.getTime();

				System.out.println("The current time in miiliseconds:"
						+ timeMilli);

				Calendar calendar = Calendar.getInstance();
				System.out.println("Current Date = " + calendar.getTime());
				System.out.println("Time in milliseconds before= "
						+ (long) calendar.getTimeInMillis());

				// Subtract 15 minutes from current date
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(Calendar.MINUTE, -5);
				System.out.println("Updated Date = " + calendar1.getTime());
				System.out.println("Time in milliseconds after = "
						+ (long) calendar1.getTimeInMillis());

				long currentfilelastmodifiedmilliseconds = (long) entry
						.getAttrs().getMTime() * 1000L;

				System.out.println("The current file time is:"
						+ currentfilelastmodifiedmilliseconds);

				if (currentfilelastmodifiedmilliseconds < calendar
						.getTimeInMillis()
						&& calendar1.getTimeInMillis() < currentfilelastmodifiedmilliseconds) {

					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;
					res = filename;
					utilityFileWriteOP.writeToLog(tcid,
							"The target file name in " + oretailpath, " is "
									+ lastmodifiedfilename, ResultPath,
							xwpfRun, "");

					System.out.println("The file is :" + filename);

					break;
				} else {

					System.out.println("There are no file generated recently");

					res = null;
				}
			}

			// br.close();
			sftpchannel.exit();

			session.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

		return res;
	}

	public static boolean ValidateFileContent(String FilePath1,
			String FileName1, String FilePath2, String FileName2, String tcid,
			String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		boolean res = false;

		BufferedReader br = null;
		BufferedReader File1Reader = null;
		BufferedReader File2Reader = null;
		String str = null;
		ArrayList<String> test1FileList = new ArrayList();
		ArrayList<String> test2FileList = new ArrayList();

		try {

			System.out.println("Inside Try");
			File1Reader = new BufferedReader(new FileReader(new File(FilePath1
					+ "/" + FileName1)));

			File2Reader = new BufferedReader(new FileReader(new File(FilePath2
					+ "/" + FileName2)));

			int k = 0;
			while ((str = File1Reader.readLine()) != null) {
				test1FileList.add(str);

				k++;

			}

			System.out.println("Outside first while" + k);

			while ((str = File2Reader.readLine()) != null) {
				test2FileList.add(str);

			}
			System.out.println("Out side second while");
			/*
			 * for(int i=0; i<test1FileList.size(); i++) {
			 * if(!test2FileList.contains(test1FileList.get(i))) { // process }
			 * else { // ignore }
			 */

			int MatchCount = 0;
			int i = 0;

			for (i = 0; i < test1FileList.size(); i++) {

				if (test1FileList.get(i).contentEquals(test2FileList.get(i))) {

					MatchCount = MatchCount + 1;

				}

				else {
					System.out.println("Condition didnot match break");

					break;

				}

			}
			System.out.println("Match count" + MatchCount);
			System.out.println("File size" + test1FileList.size());

			if (MatchCount == test1FileList.size()) {

				logger.log(LogStatus.PASS,
						" Oretail and NAS file validation success ");
				res = true;
				System.out.println("Both file content matches successfully");
				// utilityFileWriteOP.writeToLog(tcid,
				// "Oretail and NAS file validation ", " is "+"Success",
				// ResultPath, xwpfRun,"");
				return res;
			}

			else {
				res = false;
				logger.log(LogStatus.FAIL,
						"Oretail and NAS file validation failed,  Rows are not matching at line no "
								+ i);

				// utilityFileWriteOP.writeToLog(tcid,
				// "Oretail and NAS file validation ",
				// " is "+"Failed, Rows are not matching at line no "+i,
				// ResultPath, xwpfRun,"");

			}

		}

		catch (Exception e) {

			e.printStackTrace();

			// utilityFileWriteOP.writeToLog(tcid,
			// "Error occurred during File Comparison", "Due to :"+e);

			logger.log(LogStatus.FAIL,
					"Oretail and NAS file validation failed Due to " + e);

			return res;

		}

		return res;
	}

	public static int FileReader_NASPath(String filename, String filePath,
			String tcid, String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		int res = 0;
		Session session = null;
		BufferedReader br = null;

		System.out.println("File path for reading..." + filePath + "/"
				+ filename);
		try {
			// Read from local
			br = new BufferedReader(new FileReader(filePath + "/" + filename));

			String CurrentLine = null;
			int countRecord = 0;

			while ((CurrentLine = br.readLine()) != null) {

				countRecord++;

			}
			System.out.println("Number of records in file : " + countRecord);

			br.close();

			res = countRecord;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", " Due to :" + e);
			return res;
		}

		finally {
			return res;
		}

	}

	// **********Relex SFTP file validation /data/input

	public static String relexLatestFileNameExtract_PO5(String us, String pw,
			String sftp_path, String host, String port, String tcid,
			String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		String res = "";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;

		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(sftp_path);

			System.out.println("The Current path is " + sftpchannel.pwd());

			// w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel
					.ls("*openorders*.dat*");

			for (ChannelSftp.LsEntry entry : list) {

				String filename = entry.getFilename();

				System.out.println("The current file is " + filename);

				// Date currentfilelastmodifieddate = new Date(new
				// File(filename).lastModified());
				// System.out.println(entry.getAttrs().getMTime());
				long currentfilelastmodifiedmilliseconds = (long) entry
						.getAttrs().getMTime();

				if (currentfilelastmodifiedmilliseconds > lastmodifiedfiletime) {

					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;

				}
				logger.log(LogStatus.PASS, "File exists at SFTP Path");

				utilityFileWriteOP.writeToLog(tcid, "The latest file name in "
						+ sftp_path, " is " + lastmodifiedfilename, ResultPath,
						xwpfRun, "");
				// Utilities.utilityFileWriteOP.writeToLog(tcid,
				// "The latest file name in "+NASpath,
				// " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
				// Utilities.excelCellValueWrite.writeValueToCell(lastmodifiedfilename,
				// keyword, columnname, testDataPath, sheetName);

			}

			// br.close();
			sftpchannel.exit();

			session.disconnect();

			res = lastmodifiedfilename;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

		return res;
	}

	// *******File record count from SFTP download path

	public static boolean ValidateFileRecordCount_SFTPDownloadPath(
			String NASreadFileRecCount, String filename, String filePath,
			String tcid, String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		boolean res = false;
		Session session = null;
		BufferedReader br = null;

		try {
			// Read from local
			br = new BufferedReader(new FileReader(filePath + filename));

			String CurrentLine = null;
			int countRecord = 0;

			while ((CurrentLine = br.readLine()) != null) {

				// Split the Current line in tilde seperated values
				String[] strarr = CurrentLine.split("\\|");

				countRecord++;

			}
			System.out.println("Number of records in file:" + countRecord);

			br.close();

			System.out.println("File count from NAS---" + NASreadFileRecCount);

			if (Integer.parseInt(NASreadFileRecCount) == countRecord) {
				res = true;
				System.out
						.println("File record count matches with the file moved from NAS path");
				utilityFileWriteOP
						.writeToLog(tcid, "File count matched successfully",
								"File record count matches with the file moved from NAS path");

			} else {
				System.out
						.println("File record count did not matches with the file moved from NAS path");
				utilityFileWriteOP
						.writeToLog(tcid, "File count match failed",
								"File record count did not matches with the file moved from NAS path");

			}
			return res;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

	}

	// ********************Create new file at SFTP /data/input
	// path******************************************
	// ******************************************************************************************************

	public static String CreateNewFileAt_SFTP(String filename, String filePath,
			String tcid, String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		String res = null;

		BufferedReader br = null;

		try {
			// Read from local
			br = new BufferedReader(new FileReader(filePath + filename));

			int count = 0;
			String CurrentLine = null;
			String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
					.format(new Date());
			String newFile = filePath + "openorders." + timeStamp + ".dat";

			// String newFileName=newFile.substring(0,newFile.indexOf("open"));
			String newFileName = newFile.substring(newFile
					.indexOf("openorders"));

			FileWriter FW = new FileWriter(newFile, true);
			BufferedWriter BW = new BufferedWriter(FW);

			while ((CurrentLine = br.readLine()) != null) {

				// Split the Current line in tilde seperated values

				// String[] strarr = CurrentLine.split("\\|");

				count++;

				if (count < 5) {

					BW.write(CurrentLine);
					BW.newLine();

					System.out.println("Current record in file is :" + count);

					// continue;
				} else {
					System.out.println("Number of records in file:" + count);
					break;
				}

			}

			BW.close();
			br.close();

			return newFileName;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

	}

	// **************File upload to SFTP

	public static boolean nasLatestFile_SFTPUpload(String us, String pw,
			String host, String port, String sftp_path,
			String SFTP_Downloadfilepath, String newFilename, String tcid,
			String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		boolean res = false;
		JSch jsch = new JSch();
		Session session = null;

		String Result = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(sftp_path);

			System.out.println("The Current path at SFTP is : "
					+ sftpchannel.pwd());
			System.out.println("SFTP Downloadfilepath is :"
					+ SFTP_Downloadfilepath);
			System.out.println("New file name to be uploaded is :"
					+ newFilename);

			// FileInputStream fs= new FileInputStream(SFTP_Downloadfilepath +
			// newFilename);

			sftpchannel.put(SFTP_Downloadfilepath + newFilename, newFilename);

			// br.close();
			sftpchannel.exit();

			session.disconnect();

			res = true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

		return res;
	}

	// ******To get the latest file extract generated after batch within 5
	// mins(latest file )

	public static String oretailLatestSLifeFileExtract(String us, String pw,
			String oretailpath, String filePrefix, String host, String port,
			String tcid, String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
			throws IOException {

		String res = "";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(oretailpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			// w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("nb_shelf_life_"
					+ filePrefix + "_*.csv");

			String filename = "";

			int p = 0;

			for (ChannelSftp.LsEntry entry : list) {

				filename = entry.getFilename();

				System.out.println("The current file is " + filename);

				// Getting the current date
				Date time = new Date();
				// This method returns the time in millis
				long timeMilli = time.getTime();

				System.out.println("The current time in miiliseconds:"
						+ timeMilli);

				Calendar calendar = Calendar.getInstance();
				System.out.println("Current Date = " + calendar.getTime());
				System.out.println("Time in milliseconds before= "
						+ (long) calendar.getTimeInMillis());

				// Subtract 15 minutes from current date
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(Calendar.MINUTE, -30);
				System.out.println("Updated Date = " + calendar1.getTime());
				System.out.println("Time in milliseconds after = "
						+ (long) calendar1.getTimeInMillis());

				long currentfilelastmodifiedmilliseconds = (long) entry
						.getAttrs().getMTime() * 1000L;

				System.out.println("The current file time is:"
						+ currentfilelastmodifiedmilliseconds);

				if (currentfilelastmodifiedmilliseconds < calendar
						.getTimeInMillis()
						&& calendar1.getTimeInMillis() < currentfilelastmodifiedmilliseconds) {

					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;
					res = filename;
					utilityFileWriteOP.writeToLog(tcid,
							"The target file name in " + oretailpath, " is "
									+ lastmodifiedfilename, ResultPath,
							xwpfRun, "");

					System.out.println("The file is :" + filename);

					break;
				} else {

					System.out.println("There are no file generated recently");

					res = null;
				}
			}

			// br.close();
			sftpchannel.exit();

			session.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid,
					"Error occurred during File Extraction ", "Due to :" + e);
			return res;
		}

		return res;
	}

	public static boolean uploadPOSFileToNAS(String us, String pw,
			String NasUploadPath, String filename, String NasPath, String host,
			String port, String tcid, String ResultPath, XWPFRun xwpfRun,
			ExtentTest logger) {
		boolean res = false;

		System.out.println("The file name is:" + filename);

		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);

			System.out.println("The Current path at NAS is : "
					+ sftpchannel.pwd());

			System.out.println("The source path is:" + NasUploadPath);
			System.out.println(NasUploadPath + filename);
			System.out.println(NasPath + filename);

			sftpchannel.put(NasUploadPath + filename, NasPath + filename);
			System.out.println("The file " + filename
					+ " has been uploaded at " + NasPath + " successfully");
			res = true;

		} catch (Exception e) {

			System.out.println("File upload Failed " + e);

			utilityFileWriteOP.writeToLog(tcid,
					"Exception occurred while uploading file " + filename,
					" to NAS Path " + NasUploadPath, ResultPath, xwpfRun, "");

			e.printStackTrace();
			res = false;

		} finally {
			channel.disconnect();
			session.disconnect();

			return res;

		}
	}

	public static boolean validateUploadedFileToNAS(String us, String pw,
			String NasUploadPath, String filename, String NasPath, String host,
			String port, String tcid, String ResultPath, XWPFRun xwpfRun,
			ExtentTest logger) {
		boolean res = false;

		System.out.println("The file name is:" + filename);

		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);

			System.out.println("The Current path at NAS is : "
					+ sftpchannel.pwd());

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(NasPath);
			System.out.println(NasPath + filename);
			System.out.println("List" + list);

			for (int i = 0; i < 9; i++) {

				Thread.sleep(10000);
				System.out
						.println("Waiting for uploaded file to reflect at NAS path");

				if (list.lastElement().toString().contains(filename)) {
					System.out.println("Inside if " + i);
					res = true;
					break;
				} else {
					res = false;
				}
			}

		} catch (Exception e) {

			System.out.println("File upload Failed " + e);

			utilityFileWriteOP.writeToLog(tcid,
					"Exception occurred while uploading file " + filename,
					" to NAS Path " + NasUploadPath, ResultPath, xwpfRun, "");

			e.printStackTrace();
			res = false;

		} finally {
			channel.disconnect();
			session.disconnect();

			return res;

		}
	}


	public static String oretailLatestFileExtractNew(String us, String pw,String oretailpath,String filePrefix, String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {

		
		String res="";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;


			sftpchannel.cd(oretailpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			//w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			String filename="";
				
			
			int p=0;
			
			for(ChannelSftp.LsEntry entry: list) {

				 filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				
				//Getting the current date
				Date time=new Date();
				 //This method returns the time in millis
				long timeMilli = time.getTime();
				
				System.out.println("The current time in miiliseconds:"+timeMilli);
				
				Calendar calendar = Calendar.getInstance();
				System.out.println("Current Date = " + calendar.getTime());
				System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
				
				
			  // Subtract 15 minutes from current date
				Calendar calendar1 = Calendar.getInstance();
			      calendar1.add(Calendar.MINUTE, -40);
			      System.out.println("Updated Date = " + calendar1.getTime());
			      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

				
	long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
				
				
				System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);
				
				
				
	if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
					
					
					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;
					res=filename;
					//utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
					
					System.out.println("The file is :"+filename);
					
					break;
				}
				else{
					
					System.out.println("There are no file generated recently");
					
					res=null;
				}
			}
					
			//br.close();
			sftpchannel.exit();

			session.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
			return res;
		}


		return res;
	}

	
public static String SMS2FilePattern(String us, String pw,String oretailpath,String filePrefix, String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {

		
		String res="";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;


			sftpchannel.cd(oretailpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			//w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("SMS2_Holdstock_Ext_*.csv");
			
			String filename="";
				
			
			int p=0;
			
			for(ChannelSftp.LsEntry entry: list) {

				 filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				return filename;
					/*if(filename.startsWith("SMS2") && filename.contains("Holdstock") && filename.contains("Ext")){
						System.out.println("The SMS 2 file pattern validation successful for strings");
						return filename;
				}
					else{
						System.out.println("The SMS 2 file pattern validation failed for strings");
						
					}*/
			}
					
			//br.close();
			sftpchannel.exit();

			session.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
			return res;
		}


		return res;
	}

	
	
public static String fileJSON(String us, String pw,String oretailpath,String filePrefix, String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {

		
		String res="";
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		String Result = null;
		int flag = 0;

		long lastmodifiedfiletime = 0;
		String lastmodifiedfilename = null;

		try {

			session = jsch.getSession(us, host, Integer.parseInt(port));
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pw);
			session.connect();
			System.out.println("Connection successful");
			Channel channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("Channel Connection successful");
			ChannelSftp sftpchannel = (ChannelSftp) channel;


			sftpchannel.cd(oretailpath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			//w-mcc-OrderID.csv

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.json");
			
			String filename="";
				
			
			int p=0;
			
			for(ChannelSftp.LsEntry entry: list) {

				 filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				
				//Getting the current date
				Date time=new Date();
				 //This method returns the time in millis
				long timeMilli = time.getTime();
				
				System.out.println("The current time in miiliseconds:"+timeMilli);
				
				Calendar calendar = Calendar.getInstance();
				System.out.println("Current Date = " + calendar.getTime());
				System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
				
				
			  // Subtract 15 minutes from current date
				Calendar calendar1 = Calendar.getInstance();
			      calendar1.add(Calendar.MINUTE, -40);
			      System.out.println("Updated Date = " + calendar1.getTime());
			      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

				
	long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
				
				
				System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);
				
				
				
	if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
					
					
					lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
					lastmodifiedfilename = filename;
					res=filename;
					//utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
					
					System.out.println("The file is :"+filename);
					
					break;
				}
				else{
					
					System.out.println("There are no file generated recently");
					
					res=null;
				}
			}
					
			//br.close();
			sftpchannel.exit();

			session.disconnect();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
			return res;
		}


		return res;
	}

	
}


	

