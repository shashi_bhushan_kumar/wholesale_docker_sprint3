package Functions_Appworx;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import Utilities.utilityFileWriteOP;

public class BatchJoB_NFSI {

	public static boolean RunJob(String JobName, String TestCaseNo,
			String ResultPath) {

		Boolean result = false;
		try {

			// Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );

			Process p = Runtime.getRuntime().exec(
					"cscript C:/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"
							+ " " + JobName + "," + TestCaseNo + ","
							+ ResultPath);

			System.out
					.println("Waiting for UFT Script Execution to be completed ...");
			try {
				p.waitFor();

				result = true;

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		catch (IOException ex) {

		}

		finally {

			return result;

		}

	}

	public static boolean RunJob(String link, String userName, String userPwd,
			String TestCaseNo, String ResultPath, String JobName) {

		Boolean result = false;
		try {

			// Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );

			Process p = Runtime.getRuntime().exec(
					"cscript C:/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"
							+ " " + link + "," + userName + "," + userPwd + ","
							+ TestCaseNo + "," + ResultPath + "," + JobName);

			System.out
					.println("Waiting for UFT Script Execution to be completed ...");
			try {
				p.waitFor();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String batchstatus = utilityFileWriteOP
					.ReadBatchResult("C:/Appworx_Job_Run/BatchStatus.txt");
			System.out.println("BatchStatus " + batchstatus);
			if (batchstatus.equalsIgnoreCase("Finished")) {

				result = true;
			} else {
				result = false;
			}

		}

		catch (Exception ex) {

			System.out.println("Problem occurred during batch run...");
			result = false;

		}

		finally {

			return result;

		}

	}

	public static boolean RunJob(String link, String userName, String userPwd,
			String TestCaseNo, String ResultPath, String JobName,
			String batchStatusFolder) {

		Boolean result = false;
		try {

			// Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );

			Process p = Runtime.getRuntime().exec(
					"cscript C:/UFT_SCRIPTS_AmazonWholesale/Appworx_BatchParam_WS1.vbs"
							+ " " + link + "," + userName + "," + userPwd + ","
							+ TestCaseNo + "," + ResultPath + "," + JobName
							+ "," + batchStatusFolder);

			System.out
					.println("Waiting for UFT Script Execution to be completed ...");
			try {
				p.waitFor();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String batchstatus = utilityFileWriteOP
					.ReadBatchResult(batchStatusFolder);
			System.out.println("BatchStatus " + batchstatus);
			if (batchstatus.equalsIgnoreCase("Finished")) {

				result = true;
			} else {
				result = false;
			}

		}

		catch (Exception ex) {

			System.out.println("Problem occurred during batch run...");
			result = false;

		}

		finally {

			return result;

		}

	}

	public static boolean JDA_Receiving_UFT() {

		Boolean result = false;
		try {

			// Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );

			Process p = Runtime.getRuntime().exec(
					"cscript C:/Users/systcsw1/Desktop/PO/JDA_Receiving.vbs");

			System.out
					.println("Waiting for UFT Script Execution to be completed ...");
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String batchstatus = utilityFileWriteOP
					.ReadBatchResult("C:/Users/systcsw1/Desktop/PO/JDA_Status.txt");
			System.out.println("JDA Status " + batchstatus);
			if (batchstatus.equalsIgnoreCase("Completed")) {
				result = true;
			} else {
				result = false;
			}

		} catch (Exception ex) {

			System.out.println("Problem occurred during receiving...");
			result = false;

		} finally {

			return result;

		}

	}

}
